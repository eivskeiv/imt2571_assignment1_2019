<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db != null) {
            $this->db = $db;
        } else {
            try {

                $dsn = "mysql:host=". DB_HOST .";dbname=". DB_NAME .";charset=utf8";
                $this->db = new PDO($dsn, DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
            catch (PDOException $e) {
               $view = new ErrorView($e->getMessage());
               $view->create();
            }

            // TODO: Create PDO connection
        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
        $eventList = array();

        try {
            $event = $this->db->prepare("SELECT * FROM event");
            $event->execute();
            
            while ($row = $event->fetch(PDO::FETCH_ASSOC)) {
                array_push($eventList, new Event($row['title'], $row['date'], $row['description'], $row['id']));
            }

        } catch (PDOException $e) {
            $view = new ErrorView($e->getMessage());
            $view->create();
        }

        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $event = new Event('','','',$id);

        $event->verifyId($id);
        
        try {
            $row = $this->db->prepare("SELECT * FROM event WHERE id=?");
            $row->execute([$id]);
            $eventValues = $row->fetch(PDO::FETCH_ASSOC);
            $event = new Event($eventValues['title'], $eventValues['date'], $eventValues['description'], $eventValues['id']);

        } catch (PDOException $e) {
            $view = new ErrorView($e->getMessage());
            $view->create();
        }

        return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        // Make sure event contains valid data
        $event->verify(true);

        try {
            $row = $this->db->prepare("INSERT INTO event (title, date, description) VALUES (?, ?, ?)");
            $row->execute([$event->title, $event->date, $event->description]);
            $event->id = $this->db->lastInsertId();

        } catch (PDOException $e) {
            $view = new ErrorView($e->getMessage());
            $view->create();
        }
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {

        // Make sure event contains valid data
        $event->verify(true);

        try {

            $row = $this->db->prepare("UPDATE event SET title=?, date=?, description=? WHERE id=?");
            $row->execute([$event->title, $event->date, $event->description, $event->id]);

        } catch (PDOException $e) {
            $view = new ErrorView($e->getMessage());
            $view->create();
        }

    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        $event = new Event('','','',$id);
        
        $event->verifyId($id);
        
        try {
            $row = $this->db->prepare("DELETE FROM event WHERE id=?");
            $row->execute([$id]);

        } catch (PDOException $e) {
            $view = new ErrorView($e->getMessage());
            $view->create();
        }

        // TODO: Delete the event from the database
    }
}
